package com.example.meghana.medifi;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.util.HashMap;
import android.os.AsyncTask;
import com.azumio.instantheartrate.bridge.Bridge;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.azumio.instantheartrate.bridge.Bridge;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Instant Heart Rate Bridge Demo Project
 * @author Martin Sonc
 */
public class AzuActivity extends Activity {
    private Button getRate;
    private TextView heartrate;
    private TextView error;
    private Button sub;
    private Bridge bridge;

    private static final String MyPREFERENCES = "MyPref";
    //SharedPreferences sharedPreferences = PreferenceManager.getSharedPreferences(getApplicationContext());
    //SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
    SharedPreferences sharedPreferences;
    String username;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_azu);
        sub= (Button) findViewById(R.id.subhr);
        sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        username = sharedPreferences.getString("username", "none exists");
        Log.d("user from heartactivity", username);

        bridge    = new Bridge();

        getRate   = (Button)   findViewById(R.id.get_rate);
        heartrate = (TextView) findViewById(R.id.heartrate);
        error     = (TextView) findViewById(R.id.error);

        getRate.setOnClickListener(get_rate_listener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // extracts the heart rate from the result
        final int heartRate = bridge.extractRate(requestCode, resultCode, data);

        // check if an error code was returned
        if (heartRate != Bridge.ErrorCodes.RESULT_NOT_AVALILABLE) {
            // update the heart rate
            getRate.setBackground(getResources().getDrawable(R.drawable.hricon));
            sub.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        sub.setBackground(getResources().getDrawable(R.drawable.square2));
                        return true;

                    }
                    else if (event.getAction() == MotionEvent.ACTION_UP) {
                        sub.setBackground(getResources().getDrawable(R.drawable.square1));
                        new PostAsync().execute(username, String.valueOf(heartRate));

                        startActivity(new Intent(AzuActivity.this, MeaselectActivity.class));
                        return true;
                    }
                    return false;
                }
            });
            updateRate(heartRate);
        }
    }

            private View.OnClickListener get_rate_listener = new View.OnClickListener() {
        @Override

        public void onClick(View arg0) {
            // launch the Instant Heart Rate application
            getRate.setBackground(getResources().getDrawable(R.drawable.hriconc));
            bridge.launch(AzuActivity.this);
        }
    };

    private void updateRate(int rate) {
        updateError(0);
        heartrate.setText(String.valueOf(rate));
        sub.setVisibility(View.VISIBLE);

    }

    private void updateError(int error) {
        switch (error) {
            // application is not installed
            case Bridge.ErrorCodes.APPLICATION_NOT_INSTALLED:
                this.error.setText("Application not installed");
                break;

            // application needs to be updated
            case Bridge.ErrorCodes.APPLICATION_NOT_UPDATED:
                this.error.setText("Application not updated");
                break;

            // no error
            default:
                this.error.setText("");
                break;
        }
    }


class PostAsync extends AsyncTask< String, String, JSONObject> {

    JSONParser jsonParser = new JSONParser();
    private static final String LOGIN_URL = "https://mdravida.pythonanywhere.com/index/home/heartrate";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";


    @Override
    protected JSONObject doInBackground(String... args) {

        try {

            HashMap<String, String> params = new HashMap<>();

            params.put("username", args[0]);
            params.put("value", args[1]);

            Log.d("request", "starting");
            Log.d("username", args[0]);
            Log.d("value",args[1]);

            JSONObject json = jsonParser.makeHttpRequest(
                    LOGIN_URL, "POST", params);

            if (json != null) {
                Log.d("JSON result", json.toString());

                return json;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    protected void onPostExecute(JSONObject json) {

        int success = 0;
        String message = "";

        if (json != null) {
            //Toast.makeText(MainActivity.this, "Login Successful",
            //Toast.LENGTH_LONG).show();

            try {
                success = json.getInt(TAG_SUCCESS);
                message = json.getString(TAG_MESSAGE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (success == 1) {
            Toast.makeText(AzuActivity.this, "Logged heartrate!",
                    Toast.LENGTH_LONG).show();
            finish();
            Log.d("Success!", message);
        }else{
            Toast.makeText(AzuActivity.this, "Unable to log heartrate.",
                    Toast.LENGTH_LONG).show();
            Log.d("Failure", message);
        }
    }

}
}
