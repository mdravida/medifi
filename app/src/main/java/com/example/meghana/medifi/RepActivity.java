package com.example.meghana.medifi;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

import android.os.AsyncTask;
import android.util.Log;
import com.example.meghana.medifi.JSONParser;
/**
 * Created by anuragshiv on 11/19/15.
 */
public class RepActivity extends Activity {

    private static final String MyPREFERENCES = "MyPref";
    JSONParser jsonParser = new JSONParser();
    JSONArray values = new JSONArray();
    Integer category = 0;
    private static Context mContext;


    //interface to parse result
    public interface onTaskCompleted {
        void parseResult(JSONArray result);
    }

    public static Context getContext() {
        return mContext;
    }
    //class that implements interface
    public class Callback implements onTaskCompleted {
        @Override
        public void parseResult(JSONArray result) {

            TableLayout tableview = (TableLayout) findViewById(R.id.tableview);
            tableview.setPadding(15, 3, 15, 3);
            tableview.setVerticalScrollBarEnabled(true);
            switch(category)
            {
                case 1: //weight
                    Log.d("in weight", "hi");
                    TableLayout.LayoutParams lp = new TableLayout.LayoutParams(
                            TableLayout.LayoutParams.WRAP_CONTENT,
                            TableRow.LayoutParams.WRAP_CONTENT);
                    TableRow row = new TableRow(getContext());
                    row.setLayoutParams(lp);
                    row.setPadding(15, 3, 15, 3);
                    row.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    TextView Header = new TextView(getContext());
                    Header.setGravity(Gravity.CENTER);
                    Header.setText("Date Logged");
                    Header.setTextSize(25.0f);
                    Header.setPadding(15, 0, 15, 0);
                    Header.setTextColor(Color.parseColor("#09a0a5"));
                    Header.setTypeface(null, Typeface.BOLD);
                    row.addView(Header);

                    TextView Header2 = new TextView(getContext());
                    Header2.setGravity(Gravity.CENTER);
                    Header2.setText("Weight (KG)");
                    Header2.setTextSize(25.0f);
                    Header2.setPadding(70, 0, 15, 0);
                    Header2.setTextColor(Color.parseColor("#09a0a5"));
                    Header2.setTypeface(null, Typeface.BOLD);
                    row.addView(Header2);
                    tableview.addView(row);
                    for (int i = 0; i < (result.length() - 2); i++) {
                        JSONObject value = new JSONObject();
                        Double weight = 0.0 ;
                        String timestamp = new String();
                        String date = new String();
                        try {
                             value = result.getJSONObject(i);
                             weight = (double) Math.round (value.getDouble("weight") * 100) /100;
                             timestamp = value.getString("timestamp");

                             if (timestamp.equals("None"))
                                    date = timestamp;
                             else
                                    date = timestamp.substring(0,10);
                        } catch (JSONException e)
                        {
                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                        }
                        TableRow row1 = new TableRow(getContext());
                        //row1.setLayoutParams(lp);
                        row1.setPadding(15, 3, 15, 3);
                        row1.setBackgroundColor(Color.parseColor("#FFFFFF"));
                        TextView Timestamp = new TextView(getContext());
                        Timestamp.setPadding(15, 0, 15, 0);
                        Timestamp.setGravity(Gravity.CENTER);
                        Timestamp.setTextSize(15.0f);
                        Timestamp.setTextColor(Color.parseColor("#09a0a5"));
                        Timestamp.setTypeface(null, Typeface.BOLD);
                        Timestamp.setText(date);
                        row1.addView(Timestamp);
                        TextView Weight = new TextView(getContext());
                        Weight.setGravity(Gravity.CENTER);
                        Weight.setText(weight.toString());
                        Weight.setTextSize(15.0f);
                        Weight.setPadding(15, 0, 15, 0);
                        Weight.setTextColor(Color.parseColor("#09a0a5"));
                        row1.addView(Weight);
                        tableview.addView(row1);
                    }
                        break;
                case 2: //heartrate
                    Log.d("in heartrate", "hi");
                    TableLayout.LayoutParams lph = new TableLayout.LayoutParams(
                            TableLayout.LayoutParams.WRAP_CONTENT,
                            TableRow.LayoutParams.WRAP_CONTENT);
                    TableRow rowh = new TableRow(getContext());
                    rowh.setLayoutParams(lph);
                    rowh.setPadding(15, 3, 15, 3);
                    rowh.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    TextView Headerh = new TextView(getContext());
                    Headerh.setGravity(Gravity.CENTER);
                    Headerh.setText("Date Logged");
                    Headerh.setTextSize(25.0f);
                    Headerh.setPadding(15, 0, 15, 0);
                    Headerh.setTextColor(Color.parseColor("#09a0a5"));
                    Headerh.setTypeface(null, Typeface.BOLD);
                    rowh.addView(Headerh);
                    TextView Header2h = new TextView(getContext());
                    Header2h.setGravity(Gravity.CENTER);
                    Header2h.setText("Heartrate");
                    Header2h.setTextSize(25.0f);
                    Header2h.setPadding(70, 0, 15, 0);
                    Header2h.setTextColor(Color.parseColor("#09a0a5"));
                    Header2h.setTypeface(null, Typeface.BOLD);
                    rowh.addView(Header2h);
                    tableview.addView(rowh);
                    for (int i = 0; i < (result.length() - 2); i++) {
                        JSONObject value = new JSONObject();
                        Integer heartrate = 0 ;
                        String timestamp = new String();
                        String date = new String();
                        try {
                            value = result.getJSONObject(i);
                            heartrate = value.getInt("heartrate");
                            timestamp = value.getString("timestamp");

                            if (timestamp.equals("None"))
                                date = timestamp;
                            else
                                date = timestamp.substring(0,10);
                        } catch (JSONException e)
                        {
                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                        }
                        TableRow row1 = new TableRow(getContext());
                        //row1.setLayoutParams(lp);
                        row1.setPadding(15, 3, 15, 3);
                        row1.setBackgroundColor(Color.parseColor("#FFFFFF"));
                        TextView Timestamp = new TextView(getContext());
                        Timestamp.setPadding(15, 0, 15, 0);
                        Timestamp.setGravity(Gravity.CENTER);
                        Timestamp.setTextSize(15.0f);
                        Timestamp.setTextColor(Color.parseColor("#09a0a5"));
                        Timestamp.setTypeface(null, Typeface.BOLD);
                        Timestamp.setText(date);
                        row1.addView(Timestamp);
                        TextView HR = new TextView(getContext());
                        HR.setGravity(Gravity.CENTER);
                        HR.setText(heartrate.toString());
                        HR.setTextSize(15.0f);
                        HR.setPadding(15, 0, 15, 0);
                        HR.setTextColor(Color.parseColor("#09a0a5"));
                        row1.addView(HR);
                        tableview.addView(row1);
                    }

                        break;
                case 3: //bp
                    Log.d("in bp", "hi");
                    TableLayout.LayoutParams lpb = new TableLayout.LayoutParams(
                            TableLayout.LayoutParams.WRAP_CONTENT,
                            TableRow.LayoutParams.WRAP_CONTENT);
                    TableRow rowb = new TableRow(getContext());
                    rowb.setLayoutParams(lpb);
                    rowb.setPadding(15, 3, 15, 3);
                    rowb.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    TextView Headerb = new TextView(getContext());
                    Headerb.setGravity(Gravity.CENTER);
                    Headerb.setText("Date Logged");
                    Headerb.setTextSize(25.0f);
                    Headerb.setPadding(15, 0, 15, 0);
                    Headerb.setTextColor(Color.parseColor("#09a0a5"));
                    Headerb.setTypeface(null, Typeface.BOLD);
                    rowb.addView(Headerb);

                    TextView Headerb1 = new TextView(getContext());
                    Headerb1.setGravity(Gravity.CENTER);
                    Headerb1.setText("Sys");
                    Headerb1.setTextSize(25.0f);
                    Headerb1.setPadding(30, 0, 15, 0);
                    Headerb1.setTextColor(Color.parseColor("#09a0a5"));
                    Headerb1.setTypeface(null, Typeface.BOLD);
                    rowb.addView(Headerb1);

                    TextView Headerb2 = new TextView(getContext());
                    Headerb2.setGravity(Gravity.CENTER);
                    Headerb2.setText("Dia");
                    Headerb2.setTextSize(25.0f);
                    Headerb2.setPadding(15, 0, 15, 0);
                    Headerb2.setTextColor(Color.parseColor("#09a0a5"));
                    Headerb2.setTypeface(null, Typeface.BOLD);
                    rowb.addView(Headerb2);
                    tableview.addView(rowb);

                    for (int i = 0; i < (result.length() - 2); i++) {
                        JSONObject value = new JSONObject();
                        Integer sys = 0, dia = 0 ;
                        String timestamp = new String();
                        String date = new String();
                        try {
                            value = result.getJSONObject(i);
                            sys = value.getInt("systolic");
                            dia = value.getInt("diastolic");
                            timestamp = value.getString("timestamp");

                            if (timestamp.equals("None"))
                                date = timestamp;
                            else
                                date = timestamp.substring(0,10);
                        } catch (JSONException e)
                        {
                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                        }
                        TableRow row1 = new TableRow(getContext());
                        //row1.setLayoutParams(lp);
                        row1.setPadding(15, 3, 15, 3);
                        row1.setBackgroundColor(Color.parseColor("#FFFFFF"));
                        TextView Timestamp = new TextView(getContext());
                        Timestamp.setPadding(15, 0, 15, 0);
                        Timestamp.setGravity(Gravity.CENTER);
                        Timestamp.setTextSize(15.0f);
                        Timestamp.setTextColor(Color.parseColor("#09a0a5"));
                        Timestamp.setTypeface(null, Typeface.BOLD);
                        Timestamp.setText(date);
                        row1.addView(Timestamp);

                        TextView systolic = new TextView(getContext());
                        systolic.setGravity(Gravity.CENTER);
                        systolic.setText(sys.toString());
                        systolic.setTextSize(15.0f);
                        systolic.setPadding(15, 0, 15, 0);
                        systolic.setTextColor(Color.parseColor("#09a0a5"));
                        row1.addView(systolic);

                        TextView diastolic = new TextView(getContext());
                        diastolic.setGravity(Gravity.CENTER);
                        diastolic.setText(dia.toString());
                        diastolic.setTextSize(15.0f);
                        diastolic.setPadding(15, 0, 15, 0);
                        diastolic.setTextColor(Color.parseColor("#09a0a5"));
                        row1.addView(diastolic);
                        tableview.addView(row1);
                    }
                    break;
                default: break;
            }
            //finish();
        }
    }
    protected void onCreate(Bundle savedInstanceState) {
        StringBuffer activitytype = new StringBuffer("");
        super.onCreate(savedInstanceState);
        //getting context
        mContext = getApplicationContext();
        setContentView(R.layout.activity_report);
        SharedPreferences sharedPreferences;
        String username;
        sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        username = sharedPreferences.getString("username", "none exists");
        Bundle bundle = getIntent().getExtras();
        int type = bundle.getInt("type");   //Bundle data for report to generate

        switch (type) {

            case 1: //Weight
                    activitytype.append("weight");
                    category = 1;
                    break;
            case 2: //Heart Rate
                    activitytype.append("heartrate");
                    category = 2;
                    break;
            case 3: //Pressure
                    activitytype.append("bp");
                    category = 3;
                    break;
            default:break;
        }
        new GetAsync(new Callback()).execute(activitytype.toString(), username);
        Log.d("after execute", "hi");


    }


    class GetAsync extends AsyncTask<String, String, JSONObject> {


        private onTaskCompleted listener;
        StringBuilder LOGIN_URL = new StringBuilder("https://mdravida.pythonanywhere.com/index/home/");
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        Integer x= jsonParser.setReport();

        //setting up listener in constructor for callback
        public GetAsync(onTaskCompleted listener){
            this.listener=listener;
        }

        @Override
        protected JSONObject doInBackground(String... args) {

            try {
                LOGIN_URL.append(args[0]);
                //setting report to null before making request
                jsonParser.setReportValue();

                HashMap<String, String> params = new HashMap<>();
                params.put("username", args[1]);
                Log.d("final URL from REPORT",LOGIN_URL.toString());
                Log.d("report request", "starting");

                JSONObject json = jsonParser.makeHttpRequest(
                        LOGIN_URL.toString(), "GET", params);

                if (json != null) {
                    Log.d("JSON result", json.toString());

                    return json;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(JSONObject json) {

            int success = 0;
            String message = "";

            if (json != null) {

                try {
                    success = json.getInt(TAG_SUCCESS);
                    message = json.getString(TAG_MESSAGE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                Toast.makeText(RepActivity.this, "Report Got!", Toast.LENGTH_LONG).show();
                values = jsonParser.getReportValue();
                listener.parseResult(values);
                Log.d("Success!", message);
            } else {
                Toast.makeText(RepActivity.this, "Unable to display report.",
                        Toast.LENGTH_LONG).show();
                Log.d("Failure", message);
            }
        }

    }

}
