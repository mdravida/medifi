/*to access user type:
include:
import android.content.Context;
import android.content.SharedPreferences;

globally declare in your activity:
SharedPreferences sharedPreferences;
String user_type;

In particular class you are using this user type:
sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
user_type = sharedPreferences.getString("user_type", "none exists");

Now you can use the string user_type in your class.
 */

package com.example.meghana.medifi;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.MotionEvent;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Button;

import android.content.SharedPreferences;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class AddDoctorActivity extends Activity {
    EditText etResponse;
    private Button b;
    private EditText name1;
    private EditText email1;
    private EditText phone1;
    private EditText hosp1;
    private Spinner spec1;
    private String name;
    private String email;
    private String phone;
    private String hosp;
    private String spec;

    String username;

    Bundle bundle;
    private static final String MyPREFERENCES = "MyPref";
    //SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    //SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    //SharedPreferences  sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adddoctor);
        b = (Button) findViewById(R.id.sub);
        sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        editor = sharedPreferences.edit();
        bundle = getIntent().getExtras();
        username=bundle.getString("username");
        name1=(EditText) findViewById(R.id.name);
        email1=(EditText) findViewById(R.id.email);
        phone1=(EditText) findViewById(R.id.phone);
        hosp1=(EditText) findViewById(R.id.hospital);
        spec1=(Spinner) findViewById(R.id.specialization);


        b.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    b.setBackground(getResources().getDrawable(R.drawable.square2));
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    b.setBackground(getResources().getDrawable(R.drawable.square1));
                    spec= String.valueOf(spec1.getSelectedItem());
                    name=name1.getText().toString();
                    hosp=hosp1.getText().toString();
                    email=email1.getText().toString();
                    phone=phone1.getText().toString();

                    //CODE TO PUSH TO DB
                    new PostAsync().execute(username, "doctor");
                    finish();
                    return true;

                } else {
                    return false;
                }
            }
            //startActivity(new Intent(MainActivity.this,IntermediateActivity.class));

        });



    }
    class PostAsync extends AsyncTask< String, String, JSONObject> {

        JSONParser jsonParser = new JSONParser();
        private static final String LOGIN_URL = "https://mdravida.pythonanywhere.com/index/login/add/doctor";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";


        @Override
        protected JSONObject doInBackground(String... args) {

            try {

                HashMap<String, String> params = new HashMap<>();
                params.put("username", args[0]);
                params.put("name", name);
                params.put("hospital", hosp);
                params.put("speciality", spec);
                params.put("email", email);
                params.put("phone", phone);

                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(
                        LOGIN_URL, "POST", params);

                if (json != null) {
                    Log.d("JSON result", json.toString());

                    return json;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(JSONObject json) {

            int success = 0;
            String message = "";

            if (json != null) {
                //Toast.makeText(MainActivity.this, "Login Successful",
                //Toast.LENGTH_LONG).show();

                try {
                    success = json.getInt(TAG_SUCCESS);
                    message = json.getString(TAG_MESSAGE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                Toast.makeText(AddDoctorActivity.this, "Doctor created!",
                        Toast.LENGTH_LONG).show();
                finish();
                Log.d("Success!", message);
            }else{
                Toast.makeText(AddDoctorActivity.this, "Unable to add doctor.",
                        Toast.LENGTH_LONG).show();
                Log.d("Failure", message);
            }
        }

    }
}
