/*to access user type:
include:
import android.content.Context;
import android.content.SharedPreferences;

globally declare in your activity:
SharedPreferences sharedPreferences;
String user_type;

In particular class you are using this user type:
sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
user_type = sharedPreferences.getString("user_type", "none exists");

Now you can use the string user_type in your class.
 */

package com.example.meghana.medifi;

import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.View;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.Menu;
import android.view.MotionEvent;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import android.view.View.OnTouchListener;
import android.os.AsyncTask;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Button;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;

public class MainActivity extends AppCompatActivity {
    EditText etResponse;
    private Button btnLogin;
    EditText txtPassword;
    EditText txtUsername;
    ImageView user;
    ImageView pass;
    ImageView busy;
    TextView alert;
    String user_type;
    Button b1;
    EditText txtResult;
    private static final String MyPREFERENCES = "MyPref";
    //SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    //SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    //SharedPreferences  sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        editor = sharedPreferences.edit();
        user = (ImageView) findViewById(R.id.imageView);
        pass = (ImageView) findViewById(R.id.imageView2);
        alert = (TextView) findViewById(R.id.ALERT);
        busy= (ImageView) findViewById(R.id.busy);
        b1= (Button) findViewById(R.id.newu);

            File sdCard = Environment.getExternalStorageDirectory();
            File dir = new File(sdCard.getAbsolutePath() + "/Medifi");
            dir.mkdirs();

        checkLogin();

        b1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    b1.setTextColor(Color.BLACK);
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    b1.setTextColor(Color.parseColor("#0bc9d0"));
                    Intent intent= new Intent(MainActivity.this,AddActivity.class);
                    intent.putExtra("loc",0);
                    startActivity(intent);
                    return true;

                } else {
                    return false;
                }
            }
            //startActivity(new Intent(MainActivity.this,IntermediateActivity.class));

        });
    }

    class GetAsync extends AsyncTask<String, String, JSONObject> {

        JSONParser jsonParser = new JSONParser();
        private static final String LOGIN_URL = "https://mdravida.pythonanywhere.com/index/login";

        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        //SharedPreferences  sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        //SharedPreferences.Editor editor;

        @Override
        protected JSONObject doInBackground(String... args) {

            try {

                HashMap<String, String> params = new HashMap<>();
                params.put("password", args[1]);
                params.put("username", args[0]);

                Log.d("request", "starting");

                JSONObject json = jsonParser.makeHttpRequest(
                        LOGIN_URL, "GET", params);


                if (json != null) {
                    Log.d("Username", (String) json.get("username"));
                    Log.d("JSON result", json.toString());

                    return json;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(JSONObject json) {

            int success = 0;
            String message = "";


            if (json != null) {
                //Toast.makeText(MainActivity.this, json.toString(),
                  //      Toast.LENGTH_LONG).show();
                busy.setVisibility(View.VISIBLE);

                try {
                    success = json.getInt(TAG_SUCCESS);
                    message = json.getString(TAG_MESSAGE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                try {
                    editor.putString("username", json.getString("username"));
                    editor.putString("user_type", json.getString("user_type"));
                    editor.apply();
                    Log.d("sharedPref",json.getString("user_type"));
                    user_type = sharedPreferences.getString("user_type", "none exists");
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
                busy.clearAnimation();
                busy.setVisibility(View.INVISIBLE);
                Intent intent = new Intent(MainActivity.this, IntermediateActivity.class);
                intent.putExtra("check",1);
                intent.putExtra("type", user_type);
                startActivity(intent);

                finish();
                Log.d("Success!", message);
                finish();
            }else{
                busy.clearAnimation();
                busy.setVisibility(View.INVISIBLE);
                alert.setVisibility(View.VISIBLE);
                Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                vib.vibrate(500);

                final Animation an= AnimationUtils.loadAnimation(getBaseContext(), R.anim.shake);
                txtUsername.startAnimation(an);
                txtPassword.startAnimation(an);
                user.startAnimation(an);
                pass.startAnimation(an);
                Log.d("Failure", message);


            }
        }

    }

    public void checkLogin()
    {
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        txtUsername = (EditText) findViewById(R.id.txtUsername);
        //txtResult= (EditText) findViewById(R.id.etResponse);
        final Button b = (Button) findViewById(R.id.btnLogin);
        b.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    b.setBackground(getResources().getDrawable(R.drawable.square2));
                    new GetAsync().execute(txtUsername.getText().toString(), txtPassword.getText().toString());
                    return true;
                }


                else if(event.getAction() == MotionEvent.ACTION_UP){
                   b.setBackground(getResources().getDrawable(R.drawable.square1));
                    alert.setVisibility(View.INVISIBLE);
                    busy.setVisibility(View.VISIBLE);
                    final Animation an= AnimationUtils.loadAnimation(getBaseContext(), R.anim.rotate);
                    busy.startAnimation(an);

                    return true;

                }

                else{
                    return false;
                }
                }
                //startActivity(new Intent(MainActivity.this,IntermediateActivity.class));

        });
    }
}