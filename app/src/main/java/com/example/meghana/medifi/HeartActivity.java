package com.example.meghana.medifi;

import android.support.v7.app.AppCompatActivity;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.azumio.instantheartrate.bridge.Bridge;

public class HeartActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heart);

        final ImageButton ib = (ImageButton) findViewById(R.id.hr_m);
        final ImageButton report= (ImageButton) findViewById(R.id.report);

        ib.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    ib.setBackground(getResources().getDrawable(R.drawable.writec));
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    ib.setBackground(getResources().getDrawable(R.drawable.write1));
                    startActivity(new Intent(HeartActivity.this, HeartMActivity.class));
                    return true;
                }

                return false;
            }
        });

            final ImageButton ib1 = (ImageButton) findViewById(R.id.azumio);

        ib1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    ib1.setBackground(getResources().getDrawable(R.drawable.phone2c));
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    ib1.setBackground(getResources().getDrawable(R.drawable.phone2));
                    startActivity(new Intent(HeartActivity.this, AzuActivity.class));
                    return true;
                }

                return false;
            }
        });

        report.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    report.setBackground(getResources().getDrawable(R.drawable.repc));
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    report.setBackground(getResources().getDrawable(R.drawable.report));
                    Intent intent = new Intent(HeartActivity.this, RepActivity.class);
                    intent.putExtra("type", 2);
                    startActivity(intent);
                    return true;
                }

                return false;
            }
        });

        }
    }