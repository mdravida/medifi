package com.example.meghana.medifi;


        import android.net.Uri;
        import android.support.v7.app.AppCompatActivity;
        import android.annotation.TargetApi;
        import android.app.Activity;
        import android.content.Intent;
        import android.os.Build;
        import android.os.Bundle;
        import android.os.Handler;
        import android.view.MotionEvent;
        import android.view.View;
        import android.view.Window;
        import android.view.animation.Animation;
        import android.view.animation.AnimationUtils;
        import android.widget.Button;
        import android.widget.ImageButton;
        import android.widget.ImageView;
        import android.widget.TextView;
        import android.widget.Toast;
        import java.io.File;
        import java.io.FileOutputStream;
        import java.io.IOException;
        import java.text.SimpleDateFormat;
        import java.util.Date;
        import java.util.Locale;

        import android.app.Activity;
        import android.content.Intent;
        import android.graphics.Bitmap;
        import android.graphics.BitmapFactory;
        import android.os.Bundle;
        import android.os.Environment;
        import android.provider.MediaStore;
        import android.view.View;
        import android.view.View.OnClickListener;
        import android.widget.Button;
        import android.widget.ImageView;
        import android.widget.Toast;
/**
 * Created by anuragshiv on 11/19/15.
 */
public class CamActivity extends Activity {

    ImageButton ib;
    ImageButton repeatr;
    ImageButton saver;
    TextView rp;
    TextView sp;
    ImageButton pic;
    TextView picp;
    File outFile;
    String file;
    final String doctor_email="anurag.s.prasad@gmail.com";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cam);

        ib = (ImageButton) findViewById(R.id.rec);
        repeatr = (ImageButton) findViewById(R.id.repeatr);
        saver = (ImageButton) findViewById(R.id.share);
        rp = (TextView) findViewById(R.id.rp);
        sp = (TextView) findViewById(R.id.sp);
        pic = (ImageButton) findViewById(R.id.pic);
        picp = (TextView) findViewById(R.id.picp);

            Bundle bundle = getIntent().getExtras();
            int show = bundle.getInt("show");


        if(show==1) {
            file = bundle.getString("file");
            repeatr.setVisibility(View.VISIBLE);
            rp.setVisibility(View.VISIBLE);
            saver.setVisibility(View.VISIBLE);
            sp.setVisibility(View.VISIBLE);
            pic.setVisibility(View.VISIBLE);
            picp.setVisibility(View.VISIBLE);

            File sdCard = Environment.getExternalStorageDirectory();
            File dir = new File (sdCard.getAbsolutePath() + "/Medifi");
            outFile = new File(dir, file);
            repeatr.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Animation an= AnimationUtils.loadAnimation(getBaseContext(),android.R.anim.fade_in);

                    boolean deleted = outFile.delete();
                    startActivity(new Intent(CamActivity.this, CamActivity.class));
                    finish();
                }
            });



        }
        saver.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    saver.setBackground(getResources().getDrawable(R.drawable.sharec));
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    saver.setBackground(getResources().getDrawable(R.drawable.share));
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setType("text/email");
                    String aEmailList[] = { doctor_email };
                    emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, aEmailList);
                    emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(outFile));
                    startActivity(Intent.createChooser(emailIntent, "Send your email in:"));
                    return true;
                }

                return false;
            }
        });

        ib.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    ib.setBackground(getResources().getDrawable(R.drawable.camiconc));
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    ib.setBackground(getResources().getDrawable(R.drawable.camicon));
                    startActivity(new Intent(CamActivity.this, CamTestActivity.class));
                    finish();
                    return true;
                }

                return false;
            }
        });

        repeatr.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    repeatr.setBackground(getResources().getDrawable(R.drawable.repeatrc));
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    repeatr.setBackground(getResources().getDrawable(R.drawable.repeatr));
                    Intent intent = new Intent(CamActivity.this, CamActivity.class);
                    intent.putExtra("show", 0);
                    startActivity(intent);
                    return true;
                }

                return false;
            }
        });

        pic.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent1 = new Intent(CamActivity.this, PreviActivity.class);
                intent1.putExtra("file", file);
                startActivity(intent1);
            }
        });


    }
}



