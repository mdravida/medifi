package com.example.meghana.medifi;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.example.meghana.medifi.JSONParser;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PatientStatActivity extends Activity {

    private static final String MyPREFERENCES = "MyPref";
    JSONParser jsonParser = new JSONParser();
    JSONArray values = new JSONArray();
    private static Context mContext;
    SharedPreferences sharedPreferences;
    String username;
    Integer category = 0;
    Integer patientid;
    GraphView graph;
    TextView ind;
    TextView edema;
    private int flag=0;
    private double max=0;
    LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(new DataPoint[] {new DataPoint(0,0)});
    LineGraphSeries<DataPoint> series2 = new LineGraphSeries<DataPoint>(new DataPoint[] {new DataPoint(0,0)});


    public static Context getContext() {
        return mContext;
    }

    //interface to parse result
    public interface onTaskCompleted {
        void parseResult(JSONArray result);
    }

    //class that implements interface
    public class Callback implements onTaskCompleted {

        Integer idp=0;
        @Override
        public void parseResult(JSONArray result) {

            TableLayout tableview = (TableLayout) findViewById(R.id.tableview);
            tableview.removeAllViewsInLayout();
            tableview.setPadding(15, 3, 15, 3);
            tableview.setVerticalScrollBarEnabled(true);
            graph.getViewport().setScalable(true);
            graph.getViewport().setScrollable(true);
            graph.getViewport().setXAxisBoundsManual(true);
            graph.getViewport().setYAxisBoundsManual(true);
            graph.getViewport().setMaxX(30);
            graph.getViewport().setMinX(0);
            graph.getViewport().setMinY(0);
            Integer[] day = new Integer[1000];
            Integer[] month = new Integer[1000];
            Integer[] year = new Integer[1000];
            Double[] weight1= new Double[1000];
            max=0.0;
            int length=0;
            flag=1;
            String indcol="NO";
            switch (category) {
                case 1: //weight
                    Log.d("in weight", "hi");
                    graph.getViewport().setScalable(true);
                    graph.getViewport().setScrollable(true);
                    graph.getViewport().setXAxisBoundsManual(true);
                    graph.getViewport().setYAxisBoundsManual(true);
                    graph.getViewport().setMinX(0);
                    graph.getViewport().setMaxX(30);
                    graph.removeSeries(series2);
                    graph.getViewport().setMinY(0);
                    graph.getGridLabelRenderer().reloadStyles();
                    max=0.0;
                    flag=1;
                    series.resetData(new DataPoint[]{});
                    TableLayout.LayoutParams lp = new TableLayout.LayoutParams(
                            TableLayout.LayoutParams.WRAP_CONTENT,
                            TableRow.LayoutParams.WRAP_CONTENT);
                    TableRow row = new TableRow(getContext());
                    row.setLayoutParams(lp);
                    row.setPadding(15, 3, 15, 3);
                    row.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    TextView Header = new TextView(getContext());
                    Header.setGravity(Gravity.CENTER);
                    Header.setText("Date Logged");
                    Header.setTextSize(25.0f);
                    Header.setPadding(15, 0, 15, 0);
                    Header.setTextColor(Color.parseColor("#09a0a5"));
                    Header.setTypeface(null, Typeface.BOLD);
                    row.addView(Header);

                    TextView Header2 = new TextView(getContext());
                    Header2.setGravity(Gravity.CENTER);
                    Header2.setText("Weight (KG)");
                    Header2.setTextSize(25.0f);
                    Header2.setPadding(70, 0, 15, 0);
                    Header2.setTextColor(Color.parseColor("#09a0a5"));
                    Header2.setTypeface(null, Typeface.BOLD);
                    row.addView(Header2);
                    tableview.addView(row);
                    for (int i = 0; i < (result.length() - 2); i++) {
                        JSONObject value = new JSONObject();
                        Double weight = 0.0;
                        String timestamp = new String();
                        String date = new String();
                        try {
                            value = result.getJSONObject(i);
                            weight = (double) Math.round(value.getDouble("weight") * 100) / 100;
                            timestamp = value.getString("timestamp");
                            Log.d("weight", weight.toString());
                            if (timestamp.equals("None"))
                                date = timestamp;
                            else
                                date = timestamp.substring(0, 10);
                        } catch (JSONException e) {
                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                        }
                        TableRow row1 = new TableRow(getContext());
                        //row1.setLayoutParams(lp);
                        row1.setPadding(15, 3, 15, 3);
                        row1.setBackgroundColor(Color.parseColor("#FFFFFF"));
                        TextView Timestamp = new TextView(getContext());
                        Timestamp.setPadding(15, 0, 15, 0);
                        Timestamp.setGravity(Gravity.CENTER);
                        Timestamp.setTextSize(15.0f);
                        Timestamp.setTextColor(Color.parseColor("#09a0a5"));
                        Timestamp.setTypeface(null, Typeface.BOLD);
                        Timestamp.setText(date);
                        row1.addView(Timestamp);
                        TextView Weight = new TextView(getContext());
                        Weight.setGravity(Gravity.CENTER);
                        Weight.setText(weight.toString());
                        Weight.setTextSize(15.0f);
                        Weight.setPadding(15, 0, 15, 0);
                        Weight.setTextColor(Color.parseColor("#09a0a5"));
                        row1.addView(Weight);
                        tableview.addView(row1);
                        //DataPoint v= new DataPoint(i, weight);
                        //data1[i]=v;
                        if (!date.equals("None")) {
                            day[i] = Integer.parseInt(date.substring(8, 10));
                            month[i] = Integer.parseInt(date.substring(5, 7));
                            year[i] = Integer.parseInt(date.substring(0, 4));
                            weight1[i] = weight;
                            length = i;
                        }
                        else {
                            day[i] = 0;
                            month[i] = 0;
                            year[i] = 0;
                            weight1[i] = 0.0;

                        }
                            series.appendData(new DataPoint((double) i, weight), true, 500);
                            if (max < weight) {
                                max = weight;
                            }

                    }
                    for(int i=1;i<length;i++)
                    {
                            if(month[0]-month[i]==0 && year[0]-year[i]==0)
                            {
                                if(day[0]-day[i]==2)
                                {
                                    if(weight1[0]-weight1[i]>=2.0)
                                    {
                                        indcol="YES";
                                        break;
                                    }

                                    else{
                                        indcol="NO";
                                        break;
                                     }
                                }
                            }

                        else if(month[0]-month[i]==1 && year[0]-year[i]==0)
                            {
                                if(day[0]<=2 && day[i]>28)
                                {
                                    switch(month[i]){
                                        case 1:
                                            //break;
                                        case 3:
                                            //break;
                                        case 5:
                                            //break;
                                        case 7:
                                            //break;
                                        case 8:
                                            // break;
                                        case 10: if(day[i]<=31 && day[i]>=30){
                                            indcol="YES";
                                        }
                                        else{
                                            indcol="NO";
                                        }
                                            break;
                                        case 4:
                                            //break;
                                        case 6:
                                            //break;
                                        case 9:
                                            //break;
                                        case 11:
                                            if(day[i]<=30 && day[i]>=29){
                                            indcol="YES";
                                        }
                                        else{
                                            indcol="NO";
                                        }
                                            break;

                                        case 2:
                                        if(day[i]<=29 && day[i]>=27){
                                            indcol="YES";
                                        }
                                        else{
                                            indcol="NO";
                                        }
                                    }

                            }
                        }
                    }
                    ind.setText(indcol);
                    ind.setTextColor(Color.RED);
                    graph.getViewport().setMaxY(max);

                    break;
                case 2: //heartrate
                    Log.d("in heartrate", "hi");
                    graph.getViewport().setScalable(true);
                    graph.getViewport().setScrollable(true);
                    graph.getViewport().setXAxisBoundsManual(true);
                    graph.getViewport().setYAxisBoundsManual(true);
                    graph.getViewport().setMinX(0);
                    graph.getViewport().setMaxX(30);
                    graph.removeSeries(series2);
                    graph.getViewport().setMinY(0);
                    graph.getGridLabelRenderer().reloadStyles();
                    max=0.0;
                    flag=1;
                    series.resetData(new DataPoint[]{});
                    TableLayout.LayoutParams lph = new TableLayout.LayoutParams(
                            TableLayout.LayoutParams.WRAP_CONTENT,
                            TableRow.LayoutParams.WRAP_CONTENT);
                    TableRow rowh = new TableRow(getContext());
                    rowh.setLayoutParams(lph);
                    rowh.setPadding(15, 3, 15, 3);
                    rowh.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    TextView Headerh = new TextView(getContext());
                    Headerh.setGravity(Gravity.CENTER);
                    Headerh.setText("Date Logged");
                    Headerh.setTextSize(25.0f);
                    Headerh.setPadding(15, 0, 15, 0);
                    Headerh.setTextColor(Color.parseColor("#09a0a5"));
                    Headerh.setTypeface(null, Typeface.BOLD);
                    rowh.addView(Headerh);
                    TextView Header2h = new TextView(getContext());
                    Header2h.setGravity(Gravity.CENTER);
                    Header2h.setText("Heartrate");
                    Header2h.setTextSize(25.0f);
                    Header2h.setPadding(70, 0, 15, 0);
                    Header2h.setTextColor(Color.parseColor("#09a0a5"));
                    Header2h.setTypeface(null, Typeface.BOLD);
                    rowh.addView(Header2h);
                    tableview.addView(rowh);
                    for (int i = 0; i < (result.length() - 2); i++) {
                        JSONObject value = new JSONObject();
                        Integer heartrate = 0;
                        String timestamp = new String();
                        String date = new String();
                        try {
                            value = result.getJSONObject(i);
                            heartrate = value.getInt("heartrate");
                            timestamp = value.getString("timestamp");

                            if (timestamp.equals("None"))
                                date = timestamp;
                            else
                                date = timestamp.substring(0, 10);
                        } catch (JSONException e) {
                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                        }
                        TableRow row1 = new TableRow(getContext());
                        //row1.setLayoutParams(lp);
                        row1.setPadding(15, 3, 15, 3);
                        row1.setBackgroundColor(Color.parseColor("#FFFFFF"));
                        TextView Timestamp = new TextView(getContext());
                        Timestamp.setPadding(15, 0, 15, 0);
                        Timestamp.setGravity(Gravity.CENTER);
                        Timestamp.setTextSize(15.0f);
                        Timestamp.setTextColor(Color.parseColor("#09a0a5"));
                        Timestamp.setTypeface(null, Typeface.BOLD);
                        Timestamp.setText(date);
                        row1.addView(Timestamp);
                        TextView HR = new TextView(getContext());
                        HR.setGravity(Gravity.CENTER);
                        HR.setText(heartrate.toString());
                        HR.setTextSize(15.0f);
                        HR.setPadding(15, 0, 15, 0);
                        HR.setTextColor(Color.parseColor("#09a0a5"));
                        row1.addView(HR);
                        tableview.addView(row1);
                        series.appendData(new DataPoint((double) i, heartrate.doubleValue()), true, 500);
                        if(max<heartrate){max= heartrate;}

                    }
                    graph.getViewport().setMaxY(max);
                    break;
                case 3: //bp
                    Log.d("in bp", "hi");
                    graph.getViewport().setScalable(true);
                    graph.getViewport().setScrollable(true);
                    graph.getViewport().setXAxisBoundsManual(true);
                    graph.getViewport().setYAxisBoundsManual(true);
                    graph.getViewport().setMinX(0);
                    graph.getViewport().setMaxX(30);

                    graph.getViewport().setMinY(0);
                    graph.getGridLabelRenderer().reloadStyles();
                    max=0.0;
                    flag=1;
                    series.resetData(new DataPoint[]{});
                    series2.resetData(new DataPoint[]{});
                    TableLayout.LayoutParams lpb = new TableLayout.LayoutParams(
                            TableLayout.LayoutParams.WRAP_CONTENT,
                            TableRow.LayoutParams.WRAP_CONTENT);
                    TableRow rowb = new TableRow(getContext());
                    rowb.setLayoutParams(lpb);
                    rowb.setPadding(15, 3, 15, 3);
                    rowb.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    TextView Headerb = new TextView(getContext());
                    Headerb.setGravity(Gravity.CENTER);
                    Headerb.setText("Date Logged");
                    Headerb.setTextSize(25.0f);
                    Headerb.setPadding(15, 0, 15, 0);
                    Headerb.setTextColor(Color.parseColor("#09a0a5"));
                    Headerb.setTypeface(null, Typeface.BOLD);
                    rowb.addView(Headerb);

                    TextView Headerb1 = new TextView(getContext());
                    Headerb1.setGravity(Gravity.CENTER);
                    Headerb1.setText("Sys");
                    Headerb1.setTextSize(25.0f);
                    Headerb1.setPadding(30, 0, 15, 0);
                    Headerb1.setTextColor(Color.parseColor("#09a0a5"));
                    Headerb1.setTypeface(null, Typeface.BOLD);
                    rowb.addView(Headerb1);

                    TextView Headerb2 = new TextView(getContext());
                    Headerb2.setGravity(Gravity.CENTER);
                    Headerb2.setText("Dia");
                    Headerb2.setTextSize(25.0f);
                    Headerb2.setPadding(15, 0, 15, 0);
                    Headerb2.setTextColor(Color.parseColor("#09a0a5"));
                    Headerb2.setTypeface(null, Typeface.BOLD);
                    rowb.addView(Headerb2);
                    tableview.addView(rowb);

                    for (int i = 0; i < (result.length() - 2); i++) {
                        JSONObject value = new JSONObject();
                        Integer sys = 0, dia = 0;
                        String timestamp = new String();
                        String date = new String();
                        try {
                            value = result.getJSONObject(i);
                            sys = value.getInt("systolic");
                            dia = value.getInt("diastolic");
                            timestamp = value.getString("timestamp");

                            if (timestamp.equals("None"))
                                date = timestamp;
                            else
                                date = timestamp.substring(0, 10);
                        } catch (JSONException e) {
                            Log.e("JSON Parser", "Error parsing data " + e.toString());
                        }
                        TableRow row1 = new TableRow(getContext());
                        //row1.setLayoutParams(lp);
                        row1.setPadding(15, 3, 15, 3);
                        row1.setBackgroundColor(Color.parseColor("#FFFFFF"));
                        TextView Timestamp = new TextView(getContext());
                        Timestamp.setPadding(15, 0, 15, 0);
                        Timestamp.setGravity(Gravity.CENTER);
                        Timestamp.setTextSize(15.0f);
                        Timestamp.setTextColor(Color.parseColor("#09a0a5"));
                        Timestamp.setTypeface(null, Typeface.BOLD);
                        Timestamp.setText(date);
                        row1.addView(Timestamp);

                        TextView systolic = new TextView(getContext());
                        systolic.setGravity(Gravity.CENTER);
                        systolic.setText(sys.toString());
                        systolic.setTextSize(15.0f);
                        systolic.setPadding(15, 0, 15, 0);
                        systolic.setTextColor(Color.parseColor("#09a0a5"));
                        row1.addView(systolic);

                        TextView diastolic = new TextView(getContext());
                        diastolic.setGravity(Gravity.CENTER);
                        diastolic.setText(dia.toString());
                        diastolic.setTextSize(15.0f);
                        diastolic.setPadding(15, 0, 15, 0);
                        diastolic.setTextColor(Color.parseColor("#09a0a5"));
                        row1.addView(diastolic);
                        tableview.addView(row1);
                        series.appendData(new DataPoint((double) i, sys.doubleValue()), true, 500);
                        series2.appendData(new DataPoint((double) i, dia.doubleValue()), true, 500);
                        series.setColor(Color.parseColor("#000000"));
                        series2.setColor(Color.parseColor("#09a0a5"));
                        if(max<sys){max= sys;}
                    }
                    graph.getViewport().setMaxY(max);
                    graph.addSeries(series2);
                    break;
                default:
                    break;
            }

            graph.addSeries(series);

            max=0.0;
            flag=1;


        }


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patientstat);

        final Button weight = (Button) findViewById(R.id.weightstat);
        final Button heart = (Button) findViewById(R.id.heartstat);
        final Button pressure = (Button) findViewById(R.id.presstat);
        final Button deletepatient = (Button) findViewById(R.id.deletepatient);
        ind= (TextView) findViewById(R.id.ind);
        edema=(TextView) findViewById(R.id.edema);
        graph= (GraphView) findViewById(R.id.graph);


        //getting context
        mContext = getApplicationContext();
        sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        username = sharedPreferences.getString("username", "none exists");;

        Bundle bundle = getIntent().getExtras();
        patientid = bundle.getInt("patientid");

        weight.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    weight.setBackground(getResources().getDrawable(R.drawable.roundsqc));
                    heart.setBackground(getResources().getDrawable(R.drawable.roundsq1));
                    pressure.setBackground(getResources().getDrawable(R.drawable.roundsq1));
                    weight.setTextColor(Color.WHITE);
                    heart.setTextColor(Color.parseColor("#979797"));
                    pressure.setTextColor(Color.parseColor("#979797"));
                    category=1;
                    //function call
                    new GetAsync(new Callback()).execute("weight", patientid.toString());
                    ind.setVisibility(View.VISIBLE);
                    edema.setVisibility(View.VISIBLE);
                    return true;
                }


                return false;
            }

        });


        heart.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    weight.setBackground(getResources().getDrawable(R.drawable.roundsq1));
                    heart.setBackground(getResources().getDrawable(R.drawable.roundsqc));
                    pressure.setBackground(getResources().getDrawable(R.drawable.roundsq1));
                    weight.setTextColor(Color.parseColor("#979797"));
                    heart.setTextColor(Color.WHITE);
                    pressure.setTextColor(Color.parseColor("#979797"));
                    //function call
                    category=2;
                    new GetAsync(new Callback()).execute("heartrate", patientid.toString());
                    ind.setVisibility(View.INVISIBLE);
                    edema.setVisibility(View.INVISIBLE);
                    return true;
                }


                return false;
            }

        });


        pressure.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    weight.setBackground(getResources().getDrawable(R.drawable.roundsq1));
                    pressure.setBackground(getResources().getDrawable(R.drawable.roundsqc));
                    heart.setBackground(getResources().getDrawable(R.drawable.roundsq1));
                    weight.setTextColor(Color.parseColor("#979797"));
                    pressure.setTextColor(Color.WHITE);
                    heart.setTextColor(Color.parseColor("#979797"));
                    //function call
                    category=3;
                    new GetAsync(new Callback()).execute("bp", patientid.toString());
                    ind.setVisibility(View.INVISIBLE);
                    edema.setVisibility(View.INVISIBLE);
                    return true;
                }

                return false;
            }

        });

        deletepatient.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {


                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    //function call
                    deletepatient.setBackground(getResources().getDrawable(R.drawable.deletec));
                    return true;
                }

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    //function call
                    deletepatient.setText("Press Again to Confirm");
                    deletepatient.setOnTouchListener(new View.OnTouchListener() {
                        public boolean onTouch(View v1, MotionEvent event1) {


                            if (event1.getAction() == MotionEvent.ACTION_DOWN) {
                                //function call
                                deletepatient.setBackground(getResources().getDrawable(R.drawable.deletec));
                                return true;
                            }

                            if (event1.getAction() == MotionEvent.ACTION_UP) {
                                //function call
                                new PostAsync().execute(patientid.toString(), username);
                                deletepatient.setBackground(getResources().getDrawable(R.drawable.delete));
                                startActivity(new Intent(PatientStatActivity.this,PatientManActivity.class));
                                finish();
                                return true;
                            }


                            return false;
                        }

                    });
                }
                return false;
            }

        });


    }
    class GetAsync extends AsyncTask<String, String, JSONObject> {


        private onTaskCompleted listener;
        StringBuilder LOGIN_URL = new StringBuilder("https://mdravida.pythonanywhere.com/index/home/doctor/patientlist/username/");
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        Integer x= jsonParser.setReport();

        //setting up listener in constructor for callback
        public GetAsync(onTaskCompleted listener){
            this.listener=listener;
        }

        @Override
        protected JSONObject doInBackground(String... args) {

            try {
                LOGIN_URL.append(username);
                LOGIN_URL.append("/");
                LOGIN_URL.append(args[0]);
                LOGIN_URL.append("/");


                //setting report to null before making request
                jsonParser.setReportValue();

                HashMap<String, String> params = new HashMap<>();

                params.put("patientid", args[1]);

                params.put("patientid",args[1]);

                Log.d("final URL from PATIENT",LOGIN_URL.toString());
                Log.d("report request", "starting");

                JSONObject json = jsonParser.makeHttpRequest(
                        LOGIN_URL.toString(), "GET", params);

                if (json != null) {
                    Log.d("JSON result", json.toString());

                    return json;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(JSONObject json) {

            int success = 0;
            String message = "";

            if (json != null) {

                try {
                    success = json.getInt(TAG_SUCCESS);
                    message = json.getString(TAG_MESSAGE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                Toast.makeText(PatientStatActivity.this, "Report Got!", Toast.LENGTH_LONG).show();
                values = jsonParser.getReportValue();
                listener.parseResult(values);
                Log.d("Success!", message);
            } else {
                Toast.makeText(PatientStatActivity.this, "Unable to display report.",
                        Toast.LENGTH_LONG).show();
                Log.d("Failure", message);
            }
        }

    }

    class PostAsync extends AsyncTask< String, String, JSONObject> {

        JSONParser jsonParser = new JSONParser();
        private static final String LOGIN_URL = "https://mdravida.pythonanywhere.com/index/home/doctor/patientlist";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";


        @Override
        protected JSONObject doInBackground(String... args) {

            try {

                HashMap<String, String> params = new HashMap<>();

                params.put("username", args[1]);
                params.put("patientid", args[0]);
                Log.d("request", "starting");
                Log.d("username", args[0]);
                Log.d("patientid",args[1]);

                JSONObject json = jsonParser.makeHttpRequest(
                        LOGIN_URL, "POST", params);

                if (json != null) {
                    Log.d("JSON result", json.toString());

                    return json;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(JSONObject json) {

            int success = 0;
            String message = "";

            if (json != null) {
                //Toast.makeText(MainActivity.this, "Login Successful",
                //Toast.LENGTH_LONG).show();

                try {
                    success = json.getInt(TAG_SUCCESS);
                    message = json.getString(TAG_MESSAGE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                Toast.makeText(PatientStatActivity.this, "Deleted Patient!",
                        Toast.LENGTH_LONG).show();
                finish();
                Log.d("Success!", message);
            }else{
                Toast.makeText(PatientStatActivity.this, "Unable to delete patient.",
                        Toast.LENGTH_LONG).show();
                Log.d("Failure", message);
            }
        }

    }

}
