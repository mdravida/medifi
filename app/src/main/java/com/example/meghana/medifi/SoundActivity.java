package com.example.meghana.medifi;

/**
 * Created by anuragshiv on 11/18/15.
 */
import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaRecorder;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;

import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;


public class SoundActivity extends Activity {
    ImageButton play,stop,record;
    private MediaRecorder myAudioRecorder;
    private String outputFile = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sound);

        final ImageButton play=(ImageButton)findViewById(R.id.play);
        final ImageButton stop=(ImageButton)findViewById(R.id.stop);
        final ImageButton record=(ImageButton)findViewById(R.id.rec);
        final ImageButton repeatr=(ImageButton)findViewById(R.id.repeatr);
        final ImageButton saver=(ImageButton)findViewById(R.id.share);

        stop.setEnabled(false);
        play.setEnabled(false);
        final String doctor_email="anurag.s.prasad@gmail.com";

        String dir=String.format("/Medifi/%d.3gp", System.currentTimeMillis());
        outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + dir;;

        myAudioRecorder=new MediaRecorder();
        myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        myAudioRecorder.setOutputFile(outputFile);

        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    record.setBackground(getResources().getDrawable(R.drawable.miciconc));

                    myAudioRecorder.prepare();
                    myAudioRecorder.start();
                } catch (IllegalStateException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                record.setEnabled(false);
                stop.setEnabled(true);

                Toast.makeText(getApplicationContext(), "Recording started", Toast.LENGTH_LONG).show();
            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                record.setBackground(getResources().getDrawable(R.drawable.micicon));
                play.setBackground(getResources().getDrawable(R.drawable.playc));
                stop.setBackground(getResources().getDrawable(R.drawable.square2));
                repeatr.setVisibility(View.VISIBLE);
                saver.setVisibility(View.VISIBLE);
                play.setVisibility(View.VISIBLE);
                myAudioRecorder.stop();
                myAudioRecorder.release();
                myAudioRecorder = null;
                Handler myHandler = new Handler();
                myHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, 2000);
                stop.setBackground(getResources().getDrawable(R.drawable.square2));
                stop.setEnabled(false);
                play.setEnabled(true);

                Toast.makeText(getApplicationContext(), "Audio recorded successfully", Toast.LENGTH_LONG).show();
            }
        });

        repeatr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Animation an= AnimationUtils.loadAnimation(getBaseContext(),android.R.anim.fade_in);
                File file = new File(outputFile);
                boolean deleted = file.delete();
                startActivity(new Intent(SoundActivity.this, SoundActivity.class));
                finish();
            }
        });

        saver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType("text/email");
                String aEmailList[] = { doctor_email };
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, aEmailList);
                File file=new File(outputFile);
                emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
                startActivity(Intent.createChooser(emailIntent, "Send your email in:"));
            }
        });

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) throws IllegalArgumentException, SecurityException, IllegalStateException {
                play.setBackground(getResources().getDrawable(R.drawable.playc));
                MediaPlayer m = new MediaPlayer();

                try {
                    m.setDataSource(outputFile);

                }

                catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    m.prepare();
                }

                catch (IOException e) {
                    e.printStackTrace();
                }

                m.start();
                Toast.makeText(getApplicationContext(), "Playing audio", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}