/*to access user type:
include:
import android.content.Context;
import android.content.SharedPreferences;

globally declare in your activity:
SharedPreferences sharedPreferences;
String user_type;

In particular class you are using this user type:
sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
user_type = sharedPreferences.getString("user_type", "none exists");

Now you can use the string user_type in your class.
 */

package com.example.meghana.medifi;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.MotionEvent;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Button;

import android.content.SharedPreferences;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class AddPatientActivity extends Activity {

    private Button b;
    private EditText name;
    private EditText email;
    private EditText age;
    private EditText phone;
    private EditText wbase;
    private EditText hrbase;
    private EditText sysbase;
    private EditText diasbase;
    private RadioButton pat;
    private RadioButton doc;
    private String doctorname;
    private String disease;
    private String name1;
    private String age1;
    private String email1;
    private String phone1;
    private String wbase1;
    private String hrbase1;
    private String sysbase1;
    private String diasbase1;
    private int check;
    private String username;

    Bundle bundle;
    private static final String MyPREFERENCES = "MyPref";
    //SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    //SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    //SharedPreferences  sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addpatient);
        b = (Button) findViewById(R.id.sub);
        sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        editor = sharedPreferences.edit();
        sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        doctorname = sharedPreferences.getString("username", "none exists");
        doc=(RadioButton) findViewById(R.id.doctor);
        pat = (RadioButton) findViewById(R.id.copd);
        bundle = getIntent().getExtras();
        username=bundle.getString("username");
        check=bundle.getInt("check");
        name=(EditText) findViewById(R.id.name);
        email=(EditText) findViewById(R.id.email);
        phone=(EditText) findViewById(R.id.phone);
        age= (EditText) findViewById(R.id.age);
        wbase=(EditText) findViewById(R.id.weight);
        hrbase= (EditText) findViewById(R.id.hr);
        sysbase=(EditText) findViewById(R.id.sys);
        diasbase=(EditText) findViewById(R.id.dias);

        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.myRadioGroup);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.doctor) {
                    disease = new String("CHF");
                } else if (checkedId == R.id.copd) {
                    disease = new String("COPD");
                }
            }

        });


        b.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    b.setBackground(getResources().getDrawable(R.drawable.square2));
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    b.setBackground(getResources().getDrawable(R.drawable.square1));
                    name1=name.getText().toString();
                    age1=age.getText().toString();
                    phone1=phone.getText().toString();
                    email1=email.getText().toString();
                    wbase1=wbase.getText().toString();
                    hrbase1=hrbase.getText().toString();
                    sysbase1=sysbase.getText().toString();
                    diasbase1=diasbase.getText().toString();

                    //CODE TO PUSH TO DB
                    new PostAsync().execute(username, "patient");

                    //finish();
                    return true;

                } else {
                    return false;
                }
            }
            //startActivity(new Intent(MainActivity.this,IntermediateActivity.class));

        });



    }

    class PostAsync extends AsyncTask< String, String, JSONObject> {

        JSONParser jsonParser = new JSONParser();
        private static final String LOGIN_URL = "https://mdravida.pythonanywhere.com/index/login/add/patient";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";


        @Override
        protected JSONObject doInBackground(String... args) {

            try {

                HashMap<String, String> params = new HashMap<>();
                if(check ==1)
                    params.put("doctorname",doctorname);
                params.put("username", args[0]);
                params.put("name", name1);
                params.put("age", age1);
                params.put("disease", disease);
                params.put("email", email1);
                params.put("phone", phone1);
                params.put("wbase", wbase1);
                params.put("sysbase", sysbase1);
                params.put("diasbase", diasbase1);
                params.put("hrbase", hrbase1);
                // Log.d("disease",disease);

                Log.d("request", "starting");
                JSONObject json = jsonParser.makeHttpRequest(
                        LOGIN_URL, "POST", params);

                if (json != null) {
                    Log.d("JSON result", json.toString());

                    return json;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(JSONObject json) {

            int success = 0;
            String message = "";

            if (json != null) {
                //Toast.makeText(MainActivity.this, "Login Successful",
                //Toast.LENGTH_LONG).show();

                try {
                    success = json.getInt(TAG_SUCCESS);
                    message = json.getString(TAG_MESSAGE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                Toast.makeText(AddPatientActivity.this, "Patient created!",
                        Toast.LENGTH_LONG).show();
                finish();
                Log.d("Success!", message);
            }else{
                Toast.makeText(AddPatientActivity.this, "Unable to add patient.",
                        Toast.LENGTH_LONG).show();
                Log.d("Failure", message);
            }
        }

    }
}
