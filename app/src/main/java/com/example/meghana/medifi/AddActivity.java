/*to access user type:
include:
import android.content.Context;
import android.content.SharedPreferences;

globally declare in your activity:
SharedPreferences sharedPreferences;
String user_type;

In particular class you are using this user type:
sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
user_type = sharedPreferences.getString("user_type", "none exists");

Now you can use the string user_type in your class.
 */

package com.example.meghana.medifi;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Button;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class AddActivity extends Activity {
    EditText etResponse;
    private Button b;
    EditText txtPassword;
    EditText txtPasswordc;
    EditText txtUsername;
    TextView pass;
    TextView passc;
    TextView alert;
    RadioButton pat;
    RadioButton doc;
    RadioButton radioButton;
    String user_type;
    EditText txtResult;
    int check=2;
    Bundle bundle;
    private static final String MyPREFERENCES = "MyPref";
    //SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    //SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    //SharedPreferences  sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        b = (Button) findViewById(R.id.sub);
        sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        editor = sharedPreferences.edit();
        alert = (TextView) findViewById(R.id.ALERT);
        txtUsername=(EditText) findViewById(R.id.username);
        txtPassword=(EditText) findViewById(R.id.password);
        txtPasswordc=(EditText) findViewById(R.id.passwordc);
        doc=(RadioButton) findViewById(R.id.doctor);
        pat=(RadioButton) findViewById(R.id.patient);

        pass=(TextView) findViewById(R.id.pass) ;
        passc=(TextView) findViewById(R.id.passc);

        bundle = getIntent().getExtras();
        check = bundle.getInt("loc");


       final RadioGroup radioGroup = (RadioGroup) findViewById(R.id.myRadioGroup);
        Log.d("CHECK",String.valueOf(check));

        if(check==0) {
            radioGroup.setVisibility(View.VISIBLE);

        }


        b.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    b.setBackground(getResources().getDrawable(R.drawable.square2));
                    return true;
                }
                else if (event.getAction() == MotionEvent.ACTION_UP) {
                    b.setBackground(getResources().getDrawable(R.drawable.square1));
                    if (txtPassword.getText().toString().equals(txtPasswordc.getText().toString())) {
                        alert.setVisibility(View.INVISIBLE);
                        if(check==0) {
                            int selectedId = radioGroup.getCheckedRadioButtonId();
                            radioButton = (RadioButton) findViewById(selectedId);
                            // find which radio button is selected
                            if (selectedId == R.id.doctor) {
                                user_type = "Doctor";
                                Log.d("USERTYPE", user_type);
                            } else if (selectedId == R.id.patient) {
                                user_type = "Patient";
                                Log.d("USERTYPE", user_type);
                            }
                        }

                        else if(check==1){
                            user_type= "Patient";
                            Log.d("USERTYPE",user_type);

                        }

                    if(user_type.equals("Doctor")) {
                            Intent intent = new Intent(AddActivity.this,AddDoctorActivity.class);
                            intent.putExtra("username",txtUsername.getText().toString());
                            new PostAsync().execute(txtUsername.getText().toString(), txtPassword.getText().toString());
                            startActivity(intent);
                            finish();
                    }

                    else if(user_type.equals("Patient")){

                            Intent intent = new Intent(AddActivity.this,AddPatientActivity.class);
                            intent.putExtra("username", txtUsername.getText().toString());
                        intent.putExtra("check",check);
                            new PostAsync().execute(txtUsername.getText().toString(), txtPassword.getText().toString());
                            startActivity(intent);
                            finish();
                    }
                }

                else {
                    Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    vib.vibrate(500);
                    final Animation an = AnimationUtils.loadAnimation(getBaseContext(), R.anim.shake);
                    pass.setTextColor(Color.RED);
                    passc.setTextColor(Color.RED);
                    txtPassword.startAnimation(an);
                    txtPasswordc.startAnimation(an);
                    pass.startAnimation(an);
                    passc.startAnimation(an);
                    alert.setVisibility(View.VISIBLE);
                }

                return true;

                }
            else {
                    return false;
                }
            }
            //startActivity(new Intent(MainActivity.this,IntermediateActivity.class));

        });
    }

    class PostAsync extends AsyncTask< String, String, JSONObject> {

        JSONParser jsonParser = new JSONParser();
        private static final String LOGIN_URL = "https://mdravida.pythonanywhere.com/index/login/add";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";


        @Override
        protected JSONObject doInBackground(String... args) {

            try {

                HashMap<String, String> params = new HashMap<>();
                if(user_type.equals("Patient"))
                    params.put("type", "patient");
                else
                    params.put("type","doctor");
                params.put("username", args[0]);
                params.put("password", args[1]);

                Log.d("request", "starting");
                Log.d("username", args[0]);
                Log.d("password",args[1]);

                JSONObject json = jsonParser.makeHttpRequest(
                        LOGIN_URL, "POST", params);

                if (json != null) {
                    Log.d("JSON result", json.toString());

                    return json;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(JSONObject json) {

            int success = 0;
            String message = "";

            if (json != null) {
                //Toast.makeText(MainActivity.this, "Login Successful",
                //Toast.LENGTH_LONG).show();

                try {
                    success = json.getInt(TAG_SUCCESS);
                    message = json.getString(TAG_MESSAGE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                Toast.makeText(AddActivity.this, "User created!",
                        Toast.LENGTH_LONG).show();
                finish();
                Log.d("Success!", message);
            }else{
                Toast.makeText(AddActivity.this, "Unable to add user.",
                        Toast.LENGTH_LONG).show();
                Log.d("Failure", message);
            }
        }

    }
}
