package com.example.meghana.medifi;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Vibrator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import android.os.AsyncTask;

public class PressureActivity extends Activity {

    double sysint;
    double diasint;
    private static final String MyPREFERENCES = "MyPref";
    //SharedPreferences sharedPreferences = PreferenceManager.getSharedPreferences(getApplicationContext());
    //SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
    SharedPreferences sharedPreferences;
    String username;
    ImageButton report;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pressure);
        sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        username = sharedPreferences.getString("username", "none exists");
        Log.d("user from Weight", username);
        Button b1 = (Button) findViewById(R.id.sub);
        report=(ImageButton) findViewById(R.id.report);

        b1.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                final EditText sys = (EditText) findViewById(R.id.pressysnum);
                final EditText dias = (EditText) findViewById(R.id.presdiasnum);

                String sysval=sys.getText().toString();
                String diasval=dias.getText().toString();
                sysint = Double.parseDouble(sysval);
                diasint = Double.parseDouble(diasval);

                if(sysint<diasint){

                    Vibrator vib = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
                    vib.vibrate(500);

                    final Animation an= AnimationUtils.loadAnimation(getBaseContext(), R.anim.shake);
                    sys.startAnimation(an);
                    dias.startAnimation(an);
                    Toast.makeText(getApplicationContext(), "Invalid Entry! Systolic must be greater than Diastolic", Toast.LENGTH_LONG).show();
                }

                else {
                    //passing y here
                    new PostAsync().execute(username, String.valueOf(sysval), String.valueOf(diasval));
                    //new PostAsync().execute(username, String.valueOf(diasval));
                    startActivity(new Intent(PressureActivity.this, MeaselectActivity.class));
                    finish();
                }



                //pass y


            }
        });

        report.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    report.setBackground(getResources().getDrawable(R.drawable.repc));
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    report.setBackground(getResources().getDrawable(R.drawable.report));
                    Intent intent = new Intent(PressureActivity.this, RepActivity.class);
                    intent.putExtra("type", 3);
                    startActivity(intent);
                    return true;
                }

                return false;
            }
        });

    }

    class PostAsync extends AsyncTask< String, String, JSONObject> {

        JSONParser jsonParser = new JSONParser();
        private static final String LOGIN_URL = "https://mdravida.pythonanywhere.com/index/home/bp";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";


        @Override
        protected JSONObject doInBackground(String... args) {

            try {

                HashMap<String, String> params = new HashMap<>();

                params.put("username", args[0]);
                params.put("val_sys", args[1]);
                params.put("val_dia", args[2]);

                Log.d("request", "starting");
                Log.d("username", args[0]);
                Log.d("sys", args[1]);
                Log.d("dia", args[2]);

                JSONObject json = jsonParser.makeHttpRequest(
                        LOGIN_URL, "POST", params);

                if (json != null) {
                    Log.d("JSON result", json.toString());

                    return json;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(JSONObject json) {

            int success = 0;
            String message = "";

            if (json != null) {
                //Toast.makeText(MainActivity.this, "Login Successful",
                //Toast.LENGTH_LONG).show();

                try {
                    success = json.getInt(TAG_SUCCESS);
                    message = json.getString(TAG_MESSAGE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                Toast.makeText(PressureActivity.this, "Logged blood pressure!",
                        Toast.LENGTH_LONG).show();
                finish();
                Log.d("Success!", message);
            }else{
                Toast.makeText(PressureActivity.this, "Unable to log blood pressure.",
                        Toast.LENGTH_LONG).show();
                Log.d("Failure", message);
            }
        }

    }
}