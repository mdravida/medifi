package com.example.meghana.medifi;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DocHomeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dochome);

        final ImageButton ib = (ImageButton) findViewById(R.id.patient);
        final ImageButton ib1 = (ImageButton) findViewById(R.id.notes);
        final Button logout = (Button) findViewById(R.id.logout);


        ib.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    ib.setBackground(getResources().getDrawable(R.drawable.patientc));

                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    ib.setBackground(getResources().getDrawable(R.drawable.patient));
                    startActivity(new Intent(DocHomeActivity.this, PatientManActivity.class));
                    return true;
                }

                return false;
            }

        });


        ib1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    ib1.setBackground(getResources().getDrawable(R.drawable.notesc1));

                    return true;
                }

                else if (event.getAction() == MotionEvent.ACTION_UP) {
                    startActivity(new Intent(DocHomeActivity.this, NotesActivity.class));
                    ib1.setBackground(getResources().getDrawable(R.drawable.notes1));
                }

                return false;
            }

        });

        logout.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    logout.setTextColor(Color.BLACK);

                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {

                    logout.setTextColor(Color.parseColor("#09a0a5"));
                    Intent intent = new Intent(DocHomeActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }

                return false;
            }

        });

    }



}