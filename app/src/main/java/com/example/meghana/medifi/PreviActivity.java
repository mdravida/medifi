package com.example.meghana.medifi;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;

/**
 * Created by anuragshiv on 11/19/15.
 */
public class PreviActivity extends Activity {

    ImageView iv;
    File outFile;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previ);
        iv = (ImageView) findViewById(R.id.imageView);


        Bundle bundle = getIntent().getExtras();
        String file = bundle.getString("file");
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File(sdCard.getAbsolutePath() + "/Medifi");
        outFile = new File(dir, file);

        Bitmap myBitmap = BitmapFactory.decodeFile(outFile.getAbsolutePath());

        iv.setImageBitmap(myBitmap);
    }

}
