package com.example.meghana.medifi;
import android.support.v7.app.AppCompatActivity;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;


public class IntermediateActivity extends Activity {

    int check;
    Bundle bundle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_intermediate);

        bundle = getIntent().getExtras();
        check = bundle.getInt("check");

        final ImageView iv1 = (ImageView) findViewById(R.id.line);
        final ImageView iv = (ImageView) findViewById(R.id.waited);
        final Animation an1= AnimationUtils.loadAnimation(getBaseContext(), R.anim.wiperl);
        final Animation an= AnimationUtils.loadAnimation(getBaseContext(), R.anim.wipelr);
        an1.setDuration(3000);
        an.setDuration(3000);
        iv1.startAnimation(an1);
        iv.startAnimation(an);
        Thread launching = new Thread() {
            public void run() {
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        launching.start();

        Thread launching1 = new Thread() {
            public void run() {
                try {
                    sleep(3000);
                    //The if is so that you can use Intermediate between different activities.
                    if(check==1) {
                        String type = bundle.getString("type");
                        if (type.equals("Patient")) {

                            Intent i = new Intent(getApplicationContext(), MeaselectActivity.class);
                            startActivity(i);
                            finish();
                        }


                        else if(type.equals("Doctor")){

                            Intent i = new Intent(getApplicationContext(), DocHomeActivity.class);
                            startActivity(i);
                            finish();
                        }

                        else{
                            Intent i = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(i);
                            finish();
                        }

                    }


                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        launching1.start();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }

}
