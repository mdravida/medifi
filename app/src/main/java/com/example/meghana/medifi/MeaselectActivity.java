package com.example.meghana.medifi;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MeaselectActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measelect);

        final ImageButton ib = (ImageButton) findViewById(R.id.weight);
        final ImageButton ib1 = (ImageButton) findViewById(R.id.pressure);
        final ImageButton ib2 = (ImageButton) findViewById(R.id.heart);
        final ImageButton ib3 = (ImageButton) findViewById(R.id.mic);
        final ImageButton ib4 = (ImageButton) findViewById(R.id.settings);
        final ImageButton ib5 = (ImageButton) findViewById(R.id.camera);
        final Button logout = (Button) findViewById(R.id.logout);



        ib.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    ib.setBackground(getResources().getDrawable(R.drawable.weightc));

                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    ib.setBackground(getResources().getDrawable(R.drawable.weight));
                    startActivity(new Intent(MeaselectActivity.this, WeightActivity.class));
                    return true;
                }

                return false;
            }

        });


        ib1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    ib1.setBackground(getResources().getDrawable(R.drawable.pressurec));

                    return true;
                }

                else if (event.getAction() == MotionEvent.ACTION_UP) {
                    startActivity(new Intent(MeaselectActivity.this, PressureActivity.class));
                    ib1.setBackground(getResources().getDrawable(R.drawable.pressure));
                }

                return false;
            }

        });



        ib2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    ib2.setBackground(getResources().getDrawable(R.drawable.hrc));

                    return true;
                }
                else if(event.getAction() == MotionEvent.ACTION_UP){
                    ib2.setBackground(getResources().getDrawable(R.drawable.hr));
                    startActivity(new Intent(MeaselectActivity.this, HeartActivity.class));
                }

                return false;
            }

        });


        ib3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    ib3.setBackground(getResources().getDrawable(R.drawable.mic1c));

                    return true;
                }

                else if(event.getAction() == MotionEvent.ACTION_UP){
                    ib3.setBackground(getResources().getDrawable(R.drawable.mic1));
                    startActivity(new Intent(MeaselectActivity.this, SoundActivity.class));

                }

                return false;
            }

        });


            ib4.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        ib4.setBackground(getResources().getDrawable(R.drawable.settings1c));

                        return true;
                    }

                    else if(event.getAction() == MotionEvent.ACTION_UP){
                        ib4.setBackground(getResources().getDrawable(R.drawable.settings1));
                        startActivity(new Intent(MeaselectActivity.this, IntermediateActivity.class));

                    }

                    return false;
                }

            });


        ib5.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    ib5.setBackground(getResources().getDrawable(R.drawable.camerac));

                    return true;
                }

                else if(event.getAction() == MotionEvent.ACTION_UP){

                    ib5.setBackground(getResources().getDrawable(R.drawable.camera));
                    Intent intent = new Intent(MeaselectActivity.this, CamActivity.class);
                    intent.putExtra("show", 0);
                    startActivity(intent);
                }

                return false;
            }

        });

        logout.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    logout.setTextColor(Color.BLACK);

                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {

                    logout.setTextColor(Color.parseColor("#09a0a5"));
                    Intent intent = new Intent(MeaselectActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }

                return false;
            }

        });

    }
}