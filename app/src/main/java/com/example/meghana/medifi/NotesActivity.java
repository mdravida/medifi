package com.example.meghana.medifi;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


public class NotesActivity extends Activity{

    EditText ed;
    Button submit;
    private String outputFile = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_notes);
        ed= (EditText) findViewById(R.id.notes);
        submit= (Button) findViewById(R.id.submit);
        String dir=String.format("/Medifi/notes.txt");
        outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + dir;
        Log.d("filepath",outputFile);
        final File file= new File(outputFile);
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
        }catch(IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }

        ed.setText(readFromFile());
        ed.setSelection(ed.getText().length());

        submit.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    submit.setBackground(getResources().getDrawable(R.drawable.square2));
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    submit.setBackground(getResources().getDrawable(R.drawable.square1));
                    String no = ed.getText().toString();
                    writeToFile(no);
                    //passing y here
                    startActivity(new Intent(NotesActivity.this, DocHomeActivity.class));
                    finish();
                }

                return false;
            }

        });

    }

    private void writeToFile(String data) {
        try {
            File file= new File(outputFile);
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(data);
            bw.close();

        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    private String readFromFile() {
        BufferedReader br = null;
        String response = null;
        try {
            StringBuffer output = new StringBuffer();

            br = new BufferedReader(new FileReader(outputFile));
            String line = "";
            while ((line = br.readLine()) != null) {
              output.append(line +"\n");
            }
            response = output.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return response;

}}
