package com.example.meghana.medifi;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.example.meghana.medifi.JSONParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PatientManActivity extends Activity {

    private static final String MyPREFERENCES = "MyPref";
    JSONParser jsonParser = new JSONParser();
    JSONArray values = new JSONArray();
    private static Context mContext;
    SharedPreferences sharedPreferences;
    String username;
    String patientemail = new String();
    String patientphone = new String();
    Integer patientid;


    public static Context getContext() {
        return mContext;
    }

    //interface to parse result
    public interface onTaskCompleted {
        void parseResult(JSONArray result);
    }

    //class that implements interface
    public class Callback implements onTaskCompleted {

        Integer idp = 0;
        @Override
        public void parseResult(JSONArray result) {

            TableLayout tableview = (TableLayout) findViewById(R.id.tableview);
            tableview.removeAllViewsInLayout();
            tableview.setPadding(15, 3, 15, 3);
            tableview.setVerticalScrollBarEnabled(true);
            Log.d("patient list", "hi");
            TableLayout.LayoutParams lp = new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            TableRow row = new TableRow(getContext());
            row.setLayoutParams(lp);
            row.setPadding(15, 3, 15, 3);
            row.setBackgroundColor(Color.parseColor("#FFFFFF"));
            TextView Header = new TextView(getContext());
            Header.setGravity(Gravity.CENTER);
            Header.setText(" ");
            Header.setTextSize(10.0f);
            Header.setPadding(15, 0, 15, 0);
            Header.setTextColor(Color.parseColor("#09a0a5"));
            Header.setTypeface(null, Typeface.BOLD);
            row.addView(Header);

            TextView Header2 = new TextView(getContext());
            Header2.setGravity(Gravity.CENTER);
            Header2.setText(" ");
            Header2.setTextSize(10.0f);
            Header2.setPadding(70, 0, 15, 0);
            Header2.setTextColor(Color.parseColor("#09a0a5"));
            Header2.setTypeface(null, Typeface.BOLD);
            row.addView(Header2);
            tableview.addView(row);
            ArrayList pids=new ArrayList();
            for (int i = 0; i < (result.length() - 1); i++) {
                JSONObject value = new JSONObject();
                String name = new String();
                String condition = new String();

                try {
                    value = result.getJSONObject(i);
                    name = value.getString("name");
                    condition = value.getString("disease");
                    patientemail = value.getString("email");
                    patientphone = value.getString("phone");
                    idp=value.getInt("id");
                    patientid = idp;

                } catch (JSONException e) {
                    Log.e("JSON Parser", "Error parsing data " + e.toString());
                }
                TableRow row1 = new TableRow(getContext());
                //row1.setLayoutParams(lp);
                row1.setPadding(15, 3, 15, 10);
                row1.setBackgroundColor(Color.parseColor("#FFFFFF"));
                final Button Timestamp = new Button(getContext());
                Timestamp.setPadding(15, 0, 15, 0);
                Timestamp.setGravity(Gravity.TOP);
                Timestamp.setTextSize(20.0f);
                Timestamp.setTextColor(Color.parseColor("#09a0a5"));
                Timestamp.setTypeface(null, Typeface.BOLD);
                Timestamp.setText(name);
                Timestamp.setAllCaps(false);
                Timestamp.setWidth(300);
                Timestamp.setBackgroundColor(Color.parseColor("#FFFFFF"));
                Timestamp.setId(idp);
                row1.addView(Timestamp);
                TextView Weight = new TextView(getContext());
                Weight.setGravity(Gravity.BOTTOM);
                Weight.setText(condition);
                Weight.setTextSize(10.0f);
                Weight.setPadding(15, 0, 15, 0);
                Weight.setTextColor(Color.parseColor("#09a0a5"));
                row1.addView(Weight);

                final Button Email = new Button(getContext());
                Email.setPadding(15, 0, 15, 0);
                Email.setGravity(Gravity.TOP);
                Email.setTextSize(20.0f);
                Email.setTextColor(Color.parseColor("#09a0a5"));
                Email.setTypeface(null, Typeface.BOLD);
                Email.setText("Email");
                Email.setAllCaps(false);
                Email.setWidth(100);
                Email.setBackgroundColor(Color.parseColor("#FFFFFF"));
                Email.setId(i);
                row1.addView(Email);

                final Button Phone = new Button(getContext());
                Phone.setPadding(15, 0, 15, 0);
                Phone.setGravity(Gravity.TOP);
                Phone.setTextSize(20.0f);
                Phone.setTextColor(Color.parseColor("#09a0a5"));
                Phone.setTypeface(null, Typeface.BOLD);
                Phone.setText("Phone");
                Phone.setAllCaps(false);
                Phone.setWidth(100);
                Phone.setBackgroundColor(Color.parseColor("#FFFFFF"));
                Phone.setId(2 * i);
                row1.addView(Phone);


                tableview.addView(row1);
                Timestamp.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            Timestamp.setTextColor(Color.parseColor("#000000"));

                            return true;
                        } else if (event.getAction() == MotionEvent.ACTION_UP) {
                            Timestamp.setTextColor(Color.parseColor("#09a0a5"));
                            Intent intent = new Intent(PatientManActivity.this, PatientStatActivity.class);
                            intent.putExtra("patientid", Timestamp.getId());
                            startActivity(intent);
                            return true;
                        }
                        return false;
                    }

                });
                Email.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            Email.setTextColor(Color.parseColor("#000000"));

                            return true;
                        } else if (event.getAction() == MotionEvent.ACTION_UP) {
                            Email.setTextColor(Color.parseColor("#09a0a5"));
                            Intent emailIntent = new Intent(Intent.ACTION_SEND);
                            emailIntent.setType("text/email");
                            String aEmailList[] = { patientemail };
                            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, aEmailList);
                            startActivity(Intent.createChooser(emailIntent, "Send your email in:"));
                            return true;
                        }
                        return false;
                    }

                });
                Phone.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            Phone.setTextColor(Color.parseColor("#000000"));

                            return true;
                        } else if (event.getAction() == MotionEvent.ACTION_UP) {
                            Phone.setTextColor(Color.parseColor("#09a0a5"));
                            call();
                            return true;
                        }
                        return false;
                    }

                });
            }

        }
    }

    public void call() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + patientphone));
        startActivity(callIntent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patientman);

        final ImageButton ib = (ImageButton) findViewById(R.id.patientm);
        final Button active = (Button) findViewById(R.id.active);
        final Button inactive = (Button) findViewById(R.id.inactive);

        //getting context
        mContext = getApplicationContext();
        sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        username = sharedPreferences.getString("username", "none exists");

        ib.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    ib.setBackground(getResources().getDrawable(R.drawable.patientaddc));
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    ib.setBackground(getResources().getDrawable(R.drawable.patientadd1));
                    Intent intent= new Intent(PatientManActivity.this,AddActivity.class);
                    intent.putExtra("loc",1);
                    startActivity(intent);
                    return true;
                }

                return false;
            }

        });


        active.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    active.setBackground(getResources().getDrawable(R.drawable.roundsqc));
                    inactive.setBackground(getResources().getDrawable(R.drawable.roundsq1));
                    active.setTextColor(Color.WHITE);
                    inactive.setTextColor(Color.parseColor("#979797"));
                    //function call
                    new GetAsync(new Callback()).execute("active", username);

                    return true;
                }


                return false;
            }

        });


        inactive.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    active.setBackground(getResources().getDrawable(R.drawable.roundsq1));
                    inactive.setBackground(getResources().getDrawable(R.drawable.roundsqc));
                    active.setTextColor(Color.parseColor("#979797"));
                    inactive.setTextColor(Color.WHITE);
                    //function call
                    new GetAsync(new Callback()).execute("inactive", username);

                    return true;
                }

                return false;
            }

        });


    }




    class GetAsync extends AsyncTask<String, String, JSONObject> {


        private onTaskCompleted listener;
        StringBuilder LOGIN_URL = new StringBuilder("https://mdravida.pythonanywhere.com/index/home/doctor/patientlist");
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";
        Integer x = jsonParser.setReport();

        //setting up listener in constructor for callback
        public GetAsync(onTaskCompleted listener) {
            this.listener = listener;
        }

        @Override
        protected JSONObject doInBackground(String... args) {

            try {
                jsonParser.setReportValue();

                HashMap<String, String> params = new HashMap<>();
                params.put("status", args[0]);
                params.put("username", args[1]);
                Log.d("final URL from Doctor", LOGIN_URL.toString());
                Log.d("report request", "starting");

                JSONObject json = jsonParser.makeHttpRequest(
                        LOGIN_URL.toString(), "GET", params);

                if (json != null) {
                    Log.d("JSON result", json.toString());

                    return json;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(JSONObject json) {

            int success = 0;
            String message = "";

            if (json != null) {

                try {
                    success = json.getInt(TAG_SUCCESS);
                    message = json.getString(TAG_MESSAGE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                //Toast.makeText(PatientManActivity.this, "Patient List got!", Toast.LENGTH_LONG).show();
                for (int k=0; k<values.length();k++)
                    values.remove(k);
                values = jsonParser.getReportValue();
                listener.parseResult(values);
                Log.d("Success!", message);
            } else {
                //Toast.makeText(PatientManActivity.this, "Unable to display patients.",
                       // Toast.LENGTH_LONG).show();
                Log.d("Failure", message);
            }
        }

    }

}