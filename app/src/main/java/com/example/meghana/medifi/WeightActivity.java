package com.example.meghana.medifi;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ToggleButton;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import android.os.AsyncTask;
import android.util.Log;
import com.example.meghana.medifi.JSONParser;

public class WeightActivity extends Activity {

    double x;
    double y;
    private static final String MyPREFERENCES = "MyPref";
    //SharedPreferences sharedPreferences = PreferenceManager.getSharedPreferences(getApplicationContext());
    //SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
    SharedPreferences sharedPreferences;
    String username;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight);
        sharedPreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        username = sharedPreferences.getString("username", "none exists");
        Log.d("user from Weight", username);
        final Button b1 = (Button) findViewById(R.id.subw);
        final ImageButton report= (ImageButton) findViewById(R.id.report);

        b1.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    b1.setBackground(getResources().getDrawable(R.drawable.square2));
                    return true;

                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    b1.setBackground(getResources().getDrawable(R.drawable.square1));
                    final EditText weigh = (EditText) findViewById(R.id.weightnum);

                    String no = weigh.getText().toString();
                    x = Double.parseDouble(no);
                    final ToggleButton b = (ToggleButton) findViewById(R.id.unitw);

                    if (b.isChecked()) {
                        y = 0.453592 * x;
                    } else {
                        y = x;
                    }
                    //passing y here
                    new PostAsync().execute(username, String.valueOf(y));
                    startActivity(new Intent(WeightActivity.this, MeaselectActivity.class));
                    finish();
                    return true;
                }
                return false;
            }
        });

        report.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    report.setBackground(getResources().getDrawable(R.drawable.repc));
                    return true;
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    report.setBackground(getResources().getDrawable(R.drawable.report));
                    Intent intent =new Intent(WeightActivity.this, RepActivity.class);
                    intent.putExtra("type", 1);
                    startActivity(intent);
                    return true;
                }

                return false;
            }
        });
    }


    class PostAsync extends AsyncTask< String, String, JSONObject> {

        JSONParser jsonParser = new JSONParser();
        private static final String LOGIN_URL = "https://mdravida.pythonanywhere.com/index/home/weight";
        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";


        @Override
        protected JSONObject doInBackground(String... args) {

            try {

                HashMap<String, String> params = new HashMap<>();

                params.put("username", args[0]);
                params.put("value", args[1]);

                Log.d("request", "starting");
                Log.d("username", args[0]);
                Log.d("value",args[1]);

                JSONObject json = jsonParser.makeHttpRequest(
                        LOGIN_URL, "POST", params);

                if (json != null) {
                    Log.d("JSON result", json.toString());

                    return json;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(JSONObject json) {

            int success = 0;
            String message = "";

            if (json != null) {
                //Toast.makeText(MainActivity.this, "Login Successful",
                //Toast.LENGTH_LONG).show();

                try {
                    success = json.getInt(TAG_SUCCESS);
                    message = json.getString(TAG_MESSAGE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success == 1) {
                Toast.makeText(WeightActivity.this, "Logged weight!",
                        Toast.LENGTH_LONG).show();
                finish();
                Log.d("Success!", message);
            }else{
                Toast.makeText(WeightActivity.this, "Unable to log weight.",
                        Toast.LENGTH_LONG).show();
                Log.d("Failure", message);
            }
        }

    }
}