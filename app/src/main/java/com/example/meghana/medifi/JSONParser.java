package com.example.meghana.medifi;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.lang.StringBuilder;
import java.lang.String;

public class JSONParser {

    String charset = "UTF-8";
    HttpURLConnection conn;
    DataOutputStream wr;
    //StringBuilder result = new StringBuilder();
    URL urlObj;
    JSONObject jObj = null;
    StringBuilder sbParams;
    String paramsString;
    public Integer fromReport = 0;
    public JSONArray report = new JSONArray();

    public Integer setReport() {
        this.fromReport = 1;
        return 1;
    }

    public JSONObject makeHttpRequest(String url, String method,
                                      HashMap<String, String> params) {

        sbParams = new StringBuilder();
        StringBuilder result = new StringBuilder("");
        int i = 0;
        for (String key : params.keySet()) {
            try {
                if (i != 0) {
                    sbParams.append("/");
                }
                if (key.equals("val_sys"))
                    sbParams.append("sys").append("/").append(URLEncoder.encode(params.get(key), charset));
                else if (key.equals("val_dia"))
                    sbParams.append("dia").append("/").append(URLEncoder.encode(params.get(key), charset));
                else
                    sbParams.append(key).append("/").append(URLEncoder.encode(params.get(key), charset));
                //sbParams.append(key).append("/").append(params.get(key));
                //.append(URLEncoder.encode(params.get(key), charset));

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            i++;
        }
        sbParams.append("/");
        Log.d("sbParams", sbParams.toString());
        if (method.equals("POST")) {
            // request method is POST
            try {
                if (sbParams.length() != 0) {
                    url += "/" + sbParams.toString();
                }

                Log.d("final url", url);
                urlObj = new URL(url);

                conn = (HttpURLConnection) urlObj.openConnection();

                conn.setDoOutput(true);

                conn.setRequestMethod("POST");

                conn.setRequestProperty("Accept-Charset", charset);

                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);

                conn.connect();


                wr = new DataOutputStream(conn.getOutputStream());
                wr.writeBytes(url);
                wr.flush();
                wr.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (method.equals("GET")) {
            // request method is GET

            if (sbParams.length() != 0) {
                url += "/" + sbParams.toString();
            }
            paramsString = sbParams.toString();
            Log.d("final parameters", paramsString);
            try {
                urlObj = new URL(url);

                conn = (HttpURLConnection) urlObj.openConnection();

                conn.setDoOutput(false);

                conn.setRequestMethod("GET");

                conn.setRequestProperty("Accept-Charset", charset);

                conn.setConnectTimeout(15000);

                conn.connect();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        try {
            //Receive the response from the server
            InputStream in = new BufferedInputStream(conn.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }

            Log.d("JSON Parser HEREEEEEE", "result: " + result.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }

        conn.disconnect();

        // try parse the string to a JSON object
        try {

            if (fromReport == 1) {
                Log.d("from Report", fromReport.toString());
                fromReport=0;
                Log.d("Json Array result", result.toString());
                report = new JSONArray (result.toString());
                for (int j = 0; j < report.length(); j++) {
                    JSONObject value = report.getJSONObject(j);
                    Log.d("hi","in for loop");
                    Log.d("values", value.toString());
                    //String resultValue = result.toString();
                }
                jObj = new JSONObject();
                jObj.put("message", "success");
                jObj.put("success", "1");


            } else {
                jObj = new JSONObject(result.toString());

            }
        } catch (JSONException e) {
            Log.d("JSON", result.toString());
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        // return JSON Object
        return jObj;

    }

    //return report array to repActivity
    public JSONArray getReportValue()
    {
        return report;
    }

    //initialize report to empty
    public void setReportValue()
    {
        report = null;
    }

}